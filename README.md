# IoCFX

A minimal, convention over configuration oriented Inversion of Control framework designed to improve the usability of JavaFX when running it inside an OSGi container.

## What is it good for?

JavaFX is monolithic. It assumes that there is exactly one application running in the JVM that requires a UI and that the UI's life cycle is bound to that of the JVM (i.e. when the UI terminates, the entire JVM needs to be restarted to bring it back up).

In an OSGi environment, the number of applications sharing the same JVM is potentially arbitrary and so is the number of bundles that may want to bring their own UI. If every bundle, that wants to show a UI runs Application.launch(), chaos ensues.

IoCFX goes the other way round. It leaves managing the JavaFX Platform to the framework and allows clients to just publish their desire to be assigned a UI. IoCFX will take care of that, once it has initialized the JavaFX Platform.

Also since version 3.1.0 IoCFX includes the concept of Views, which allow individual modules of the UI to be defined in their own loosely coupled bundles. The framework takes care of placing them at the correct position within the UI, again following a convention over configuration approach, without the need to write any extra code or introducing hard dependencies. The MultiViewApp example shows how to use Views.

## Why use it?

IoCFX was designed with maximum simplicity in mind and follows a strict convention-over-configuration approach. If you already have an OSGi framework running and want it to have a UI, let your UI component implement Standalone.Application or Fxml.Application and make sure that the net.bbmsoft.iocfx bundle is installed in the framework. Voilà, your UI bundle will be put into a Stage and shown as soon as it gets activated.

## Why not use it?

IoCFX is designed to be very simple. There are other projects out there that do quite similar things but offer more configurability or extra functionality. Not doing so is a deliberate choice in IoCFX. The goal here is to not add anything to your dependency tree that you don't actually use.
However if you do plan to use the additional flexibility other projects offer, they may of course be a better choice for you.

## How can I use it?

IoCFX is hosted on [maven central](https://mvnrepository.com/artifact/net.bbmsoft/iocfx). Just add it to your maven dependencies.

## Which Java versions will it run with?

#### Java 7 and older:

No.

#### Java 8

Verions 3.1.0 and older are fully compatible with Java 8 and any reasonably recent version of JavaFX 8.

#### Java 9 and 10

Verions 3.1.0 and older should run on Java 9 and 10, however no exhaustive testing has been done here on my side.

#### Java 11

Versions 4.0.0 and later are specifically designed to be compatible with OpenJDK 11 and JavaFX 11 and newer.

## How do I build it?

```
git clone https://gitlab.com/bbmsoft.net/iocfx.git
cd iocfx
mvn clean install
```

This obviously requires maven and JavaFX.

## How do I run it?

IoCFX is not executable by itself. However the repository includes some small executable example.

To run it, follow these simple steps:

#### With eclipse:

 - make sure you have the Bndtools Plugin 4.1.0+ installed
 - import all four projects contained in the repository into your workspace as maven projects
 - open iocfx-example/iocfx-example.bndrun and click 'Run OSGi'

#### Without eclipse:

I'm using Apache Felix here since that's the OSGi framework I'm most familiar with, but any other framework implementing OSGi  R6 should work as well.

I'm assuming you cloned th IoCFX repo into /tmp. Please adjust this path accordingly!

 - follow the [How do I build it?](#how-do-i-build-it) instructions
 - download Apache Felix from [the felix website](http://felix.apache.org/downloads.cgi)
 - add the following packages to the ```org.osgi.framework.system.packages.extra``` property in conf/config.properties: ```javafx.animation, javafx.application, javafx.beans.property, javafx.collections, javafx.event, javafx.fxml, javafx.scene, javafx.scene.control, javafx.scene.layout, javafx.stage```
 
 - start felix according to its documentation (usually by running ```java -jar bin/felix.jar``` from its root directory)
 - in the Felix Gogo shell that should come up, type ```install http://central.maven.org/maven2/org/apache/felix/org.apache.felix.scr/2.0.14/org.apache.felix.scr-2.0.14.jar``` and hit enter
 - start the bundle with the ID the shell just printed out by typing ```start <ID>``` and hitting enter
 - type ```install file:/tmp/iocfx/iocfx/target/iocfx-3.1.0.jar```
 - start the bundle with the ID the shell just printed out by typing ```start <ID>``` and hitting enter
 - type ```install file:/tmp/iocfx/iocfx-example/target/iocfx-example-3.1.0.jar```
 - start the bundle with the ID the shell just printed out by typing ```start <ID>``` and hitting enter
 
You should see a window with a short greeting message popping up.

There are some other examples contained in the repo, you will find them in the package ```net.bbmsoft.iocfx.osgi.example```. Change the ```enabled``` flag in their ```@Component``` annotation to ```true``` in order to run them.
