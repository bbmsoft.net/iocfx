package net.bbmsoft.iocfx.osgi.example;

import org.osgi.service.component.annotations.Component;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.bbmsoft.iocfx.Fxml;

/**
 * Simple demo UI that implements {@link Fxml.Application}. This will create a
 * Stage containing the content of FxmlApplicationUIApp.fxml without any further
 * steps necessary.
 * <p>
 * This example also shows how to customize the stage style.
 * 
 * @author Michael Bachmann
 *
 */
// Note that this class has no method to provide a path to an FXML file. That's
// because by default, IoCFX will look for an FXML file with the same name as
// the class in the same package, in this case
// /net/bbmsoft/iocfx/osgi/example/FxmlApplicationUI.fxml
@Component(enabled = false)
public class FxmlApplicationUIApp implements Fxml.Application, Fxml.Controller {

	private Stage stage;

	// This method is always called on the JavaFX Application thread before the
	// stage is shown for the first time. It has a default implementation that does
	// nothing, so overriding it is optional.
	@Override
	public void prepareStage(Stage primaryStage) {
		this.stage = primaryStage;
		this.stage.initStyle(StageStyle.UNDECORATED);
	}

	// Called from the button defined in
	// /net/bbmsoft/iocfx/osgi/example/FxmlApplicationUI.fxml
	@FXML
	void quit() {
		this.stage.hide();
	}

}
