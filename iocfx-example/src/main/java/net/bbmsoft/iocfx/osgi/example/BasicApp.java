package net.bbmsoft.iocfx.osgi.example;

import org.osgi.service.component.annotations.Component;

import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import net.bbmsoft.iocfx.Standalone;

/**
 * Most basic example. This is all you need to do in order to create and start a
 * UI with IoCFX.
 * 
 * @author Michael Bachmann
 *
 */
@Component(enabled = false)
public class BasicApp implements Standalone.Application {

	@Override
	public Region getRootNode() {
		return new Label("Hello, IoCFX!\nClosing this Window will exit the application.");
	}
}
