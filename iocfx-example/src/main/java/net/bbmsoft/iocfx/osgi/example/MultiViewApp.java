package net.bbmsoft.iocfx.osgi.example;

import org.osgi.service.component.annotations.Component;

import net.bbmsoft.iocfx.Fxml;
import net.bbmsoft.iocfx.view.View;

/**
 * A slightly more complex demo UI that implements {@link Fxml.Application} and
 * makes use of {@link View Views} to modularize individual UI components.
 * <p>
 * As can be seen in MultiViewApp.fxml, the main UI merely serves as a container
 * for two views defined in the net.bbmsoft.iocfx.osgi.example.multiview
 * package. IoCFX Views will automatically put these views in the respective
 * parent panes in the main UI.
 * <p>
 * Refer to {@link FxmlApplicationUIApp} for an {@link Fxml.Application} example
 * that does not use Views.
 * 
 * @author Michael Bachmann
 *
 */
@Component(enabled = true)
public class MultiViewApp implements Fxml.Application {

}
