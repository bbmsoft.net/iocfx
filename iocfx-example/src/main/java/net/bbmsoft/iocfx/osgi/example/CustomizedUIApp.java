package net.bbmsoft.iocfx.osgi.example;

import java.net.URL;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;

import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import net.bbmsoft.iocfx.Fxml;
import net.bbmsoft.iocfx.StageService;
import net.bbmsoft.iocfx.StageService.ExitPolicy;
import net.bbmsoft.iocfx.Standalone;

/**
 * Simple demo UI that uses a {@link StageService} to acquire its stage, that
 * provides a custom FXML location ad that implements the {@link Consumer}
 * interface to receive the layout instance loaded from FXML.
 * 
 * @deprecated This was how the first release of IoCFX worked, however with
 *             newer versions using this approach is not recommended anymore.
 *             Use the {@link Standalone.Application} and
 *             {@link Fxml.Application} interfaces instead. Check out
 *             {@link BasicApp} and {@link FxmlApplicationUIApp} for examples.
 * 
 * @author Michael Bachmann
 *
 */
@Deprecated
@Component(enabled = false)
public class CustomizedUIApp implements Fxml.Consumer<Region> {

	// The PROTOTYPE_REQUIRED scope makes sure that no other instance receives the
	// same StageService/Stage object
	@Deprecated
	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private StageService stageService;

	// This is where the UI receives the layout loaded from FXML. This method will
	// always be called on the JavaFX Application Thread
	@Override
	public void accept(Region root) {

		this.stageService.setExitPolicy(ExitPolicy.DO_NOTHING_ON_STAGE_EXIT);

		Stage stage = this.stageService.getStage();
		stage.setScene(new Scene(root));
		stage.show();
	}

	// Normally, IoCFX would try to load
	// /net/bbmsoft/iocfx.osgi/example/CustomizedUI.fxml; by overriding the
	// getLocation() method, a custom location can be provided.
	@Override
	public URL getLocation() {
		return this.getClass().getResource("FxmlExample.fxml");
	}

}
