package net.bbmsoft.iocfx.osgi.example;

import java.net.URL;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import javafx.fxml.Initializable;

/**
 * Simple demo component that does not provide a UI, but is still initialized on
 * the JavaFX Application Thread as soon the JavaFX Platform is available.
 * 
 * @author Michael Bachmann
 *
 */
// Services providing the Initializable interface will automatically be found
// and initialized by IoCFX.
@Component(enabled = false)
public class SimpleInitializable implements Initializable {

	// This will always be called on the JavaFX Application Thread.
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Hello from " + Thread.currentThread().getName());
	}

}
