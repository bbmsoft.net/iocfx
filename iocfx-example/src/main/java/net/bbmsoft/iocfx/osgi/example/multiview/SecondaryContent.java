package net.bbmsoft.iocfx.osgi.example.multiview;

import org.osgi.service.component.annotations.Component;

import javafx.scene.layout.StackPane;
import net.bbmsoft.iocfx.Fxml;
import net.bbmsoft.iocfx.osgi.example.MultiViewApp;
import net.bbmsoft.iocfx.view.View;

/**
 * View component of the {@link MultiViewApp} example.
 * 
 * @author Michael Bachmann
 *
 */
@Component(enabled = true)
public class SecondaryContent extends StackPane implements View, Fxml.Root {

}
