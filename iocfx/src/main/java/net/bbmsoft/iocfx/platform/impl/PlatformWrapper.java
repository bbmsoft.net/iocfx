package net.bbmsoft.iocfx.platform.impl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanProperty;

public class PlatformWrapper implements net.bbmsoft.iocfx.Platform {
	
	private volatile boolean active;

	private ScheduledExecutorService platformScheduler;

	@Override
	public ReadOnlyBooleanProperty accessibilityActiveProperty() {
		return Platform.accessibilityActiveProperty();
	}

	@Override
	public boolean isAccessibilityActive() {
		return Platform.isAccessibilityActive();
	}

	@Override
	public void exit() {
		Platform.exit();
	}

	@Override
	public boolean isFxApplicationThread() {
		return Platform.isFxApplicationThread();
	}

	@Override
	public boolean isImplicitExit() {
		return Platform.isImplicitExit();
	}

	@Override
	public boolean isSupported(ConditionalFeature feature) {
		return Platform.isSupported(feature);
	}

	@Override
	public void runLater(Runnable runnable) {
		
		if(this.active) {
			return;
		}
		
		Platform.runLater(runnable);
	}

	@Override
	public void setImplicitExit(boolean implicitExit) {
		Platform.setImplicitExit(implicitExit);
	}

	@Override
	public void runAndWait(Runnable runnable) throws InterruptedException {

		if (this.isFxApplicationThread()) {
			runnable.run();
			return;
		}
		
		if(this.active) {
			return;
		}

		CountDownLatch latch = new CountDownLatch(1);

		this.runLater(() -> {
			try {
				runnable.run();
			} finally {
				latch.countDown();
			}
		});

		latch.await();
	}

	@Override
	public void runOnFxApplicationThread(Runnable runnable) {

		if (this.isFxApplicationThread()) {
			runnable.run();
		} else {
			this.runLater(runnable);
		}
	}

	@Override
	public ScheduledFuture<Void> runOnFxApplicationThread(Runnable runnable, long delay, TimeUnit unit) {

		return this.getPlatformScheduler().schedule(() -> {
			this.runLater(runnable);
			return null;
		}, delay, unit);
	}

	@Override
	public void assertFxApplicationThread() {

		// TODO check config

		if (!this.isFxApplicationThread()) {
			throw new IllegalStateException(
					"Not on FX Application Thread. Current thread is " + Thread.currentThread().getName());
		}
	}

	public synchronized void stop() {
		
		this.active = false;

		if (this.platformScheduler != null) {
			this.platformScheduler.shutdown();
		}
	}

	private ScheduledExecutorService getPlatformScheduler() {

		ScheduledExecutorService scheduler = this.platformScheduler;

		if (scheduler == null) {
			scheduler = this.initPlatformScheduler();
		}

		return scheduler;
	}

	private synchronized ScheduledExecutorService initPlatformScheduler() {

		if (this.platformScheduler == null) {
			this.platformScheduler = Executors
					.newSingleThreadScheduledExecutor(r -> new Thread(r, "IoCFX Platform Scheduler Thread"));
		}

		return this.platformScheduler;
	}

}
