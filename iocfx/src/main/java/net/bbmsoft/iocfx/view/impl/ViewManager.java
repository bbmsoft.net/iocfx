package net.bbmsoft.iocfx.view.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import net.bbmsoft.iocfx.Fxml;
import net.bbmsoft.iocfx.Platform;
import net.bbmsoft.iocfx.Standalone;
import net.bbmsoft.iocfx.fxml.impl.ApplicationManager;
import net.bbmsoft.iocfx.view.View;

@Component
public class ViewManager {

	private final List<Fxml.Application> fxmlApplications;
	private final List<Standalone.Application> standaloneApplications;

	private final Object viewLock;
	private Queue<View> viewsPendingIntegration;
	private Queue<View> viewsPendingRemoval;

	private final AnimationTimer timer;

	@Reference
	private Platform platform;

	@Reference
	private ApplicationManager appManager;

	public ViewManager() {

		this.fxmlApplications = new CopyOnWriteArrayList<>();
		this.standaloneApplications = new CopyOnWriteArrayList<>();

		this.viewLock = new Object();
		this.viewsPendingIntegration = new LinkedList<>();
		this.viewsPendingRemoval = new LinkedList<>();

		this.timer = new AnimationTimer() {

			@Override
			public void handle(long now) {
				synchronized (ViewManager.this.viewLock) {
					ViewManager.this.intergrateViews();
					ViewManager.this.removeViews();
				}
			}
		};
	}

	@Activate
	public void activate() {
		this.platform.runOnFxApplicationThread(this.timer::start);
	}

	@Deactivate
	public void deactivate() {
		this.platform.runOnFxApplicationThread(this.timer::stop);
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public void addFxmlApplication(Fxml.Application application) {
		this.fxmlApplications.add(application);
	}

	public void removeFxmlApplication(Fxml.Application application) {
		this.fxmlApplications.remove(application);
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public void addStandaloneApplication(Standalone.Application application) {
		this.standaloneApplications.add(application);
	}

	public void removeStandaloneApplication(Standalone.Application application) {
		this.standaloneApplications.remove(application);
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public void addView(View view) {
		synchronized (this.viewLock) {
			this.viewsPendingIntegration.add(view);
		}
	}

	public void removeView(View view) {
		synchronized (this.viewLock) {
			this.viewsPendingIntegration.remove(view);
			this.viewsPendingRemoval.add(view);
		}
	}

	protected void intergrateViews() {
		
		Queue<View> viewsPendingIntegration = this.viewsPendingIntegration;
		this.viewsPendingIntegration = new LinkedList<>();
		
		for(View view : viewsPendingIntegration) {
			this.integrateView(view);
		}
	}

	protected void removeViews() {

		Queue<View> viewsPendingRemoval = this.viewsPendingRemoval;
		this.viewsPendingRemoval = new LinkedList<>();
		
		for(View view : viewsPendingRemoval) {
			this.desintegrateView(view);
		}
	}

	private void integrateView(View view) {

		for (Fxml.Application app : this.fxmlApplications) {
			Parent parent = this.appManager.getRootNode(app);
			if (integrateView(view, parent)) {
				return;
			}
		}

		for (Standalone.Application app : this.standaloneApplications) {
			Parent parent = this.appManager.getRootNode(app);
			if (integrateView(view, parent)) {
				return;
			}
		}

		// no parent found, but it may just not have been created yet, so the view is
		// reinserted into the loop to try again later
		this.viewsPendingIntegration.add(view);
	}

	private void desintegrateView(View view) {

		for (Fxml.Application app : this.fxmlApplications) {
			Parent parent = this.appManager.getRootNode(app);
			if (removeView(view, parent)) {
				return;
			}
		}

		for (Standalone.Application app : this.standaloneApplications) {
			Parent parent = this.appManager.getRootNode(app);
			if (removeView(view, parent)) {
				return;
			}
		}
	}

	private boolean integrateView(View view, Node root) {
		
		Node parent = root.lookup(view.getParentID());

		if (parent == null) {
			return false;
		} else if (parent instanceof Pane) {
			((Pane) parent).getChildren().add(view.getNode());
			return true;
		} else {
			throw new IllegalStateException(
					"Parent must be derived from " + Pane.class.getName() + " but is a " + parent.getClass().getName());
		}
	}

	private boolean removeView(View view, Node root) {
		
		Node parent = root.lookup(view.getParentID());

		if (parent instanceof Pane) {
			return ((Pane) parent).getChildren().remove(view.getNode());
		}

		return false;
	}
}
