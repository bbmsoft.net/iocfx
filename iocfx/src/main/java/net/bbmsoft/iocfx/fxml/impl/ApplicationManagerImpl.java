package net.bbmsoft.iocfx.fxml.impl;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferenceScope;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import net.bbmsoft.iocfx.Fxml;
import net.bbmsoft.iocfx.Platform;
import net.bbmsoft.iocfx.StageService;
import net.bbmsoft.iocfx.StageService.ExitPolicy;
import net.bbmsoft.iocfx.Standalone;

@Component
public class ApplicationManagerImpl implements ApplicationManager {

	private final Map<Standalone, Stage> stages;
	private final Set<Standalone.Application> apps;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<StageService> stageService;

	@Reference
	private Platform platform;

	private boolean active;

	public ApplicationManagerImpl() {
		this.stages = new IdentityHashMap<>();
		this.apps = new HashSet<>();
	}

	@Activate
	public synchronized void activate() {

		this.active = true;

		for (Standalone.Application app : this.apps) {
			startOnFxmlThread(app);
		}
	}

	@Deactivate
	public synchronized void deactivate() {
		
		this.active = false;
		
		for (Standalone.Application app : this.apps) {
			stopOnFxmlThread(app);
		}
		
		this.apps.clear();
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	public synchronized void addApplication(Standalone.Application application) {

		if (this.apps.add(application) && this.active) {
			startOnFxmlThread(application);
		}
	}
	
	@Override
	public Parent getRootNode(Standalone application) {
		
		this.platform.assertFxApplicationThread();
		
		Stage stage = this.stages.get(application);
		if(stage == null) {
			return null;
		}
		
		Scene scene = stage.getScene();
		if(scene == null) {
			return null;
		}
		
		return scene.getRoot();
	}

	public synchronized void removeApplication(Standalone.Application application) {

		if (this.apps.remove(application) && this.active) {
			stopOnFxmlThread(application);
		}
	}

	@Override
	public void addFxmlApplication(Fxml.Application fxmlApp, Region root) {
		this.platform.runOnFxApplicationThread(() -> doAddApplication(fxmlApp, root));
	}

	@Override
	public void removeFxmlApplication(Fxml.Application fxmlApp) {
		this.platform.runOnFxApplicationThread(() -> doRemoveApplication(fxmlApp));
	}

	private void startOnFxmlThread(Standalone.Application app) {
		this.platform.runOnFxApplicationThread(() -> doAddApplication(app));
	}

	private void stopOnFxmlThread(Standalone.Application app) {
		this.platform.runOnFxApplicationThread(() -> doRemoveApplication(app));
	}

	private void doAddApplication(Standalone.Application application) {
		this.doAddApplication(application, application.getRootNode());
	}

	private void doAddApplication(Standalone app, Region root) {

		if (root == null) {
			return;
		}

		StageService stageService = this.stageService.getService();
		Stage stage = stageService.getStage();

		this.stages.put(app, stage);

		ExitPolicy exitPolicy = app.getExitPolicy();
		exitPolicy = exitPolicy != null ? exitPolicy : ExitPolicy.SHUTDOWN_ON_STAGE_EXIT;
		stageService.setExitPolicy(exitPolicy, app.getClass());
		stage.setScene(new Scene(root));
		app.prepareStage(stage);
		stage.show();
	}

	private void doRemoveApplication(Standalone application) {

		Stage stage = this.stages.remove(application);

		if (stage != null) {
			stage.hide();
		}
	}
}
