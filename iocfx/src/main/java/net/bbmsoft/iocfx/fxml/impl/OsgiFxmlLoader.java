package net.bbmsoft.iocfx.fxml.impl;

import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.ServiceScope;

import javafx.fxml.FXMLLoader;

/**
 * An {@link FXMLLoader} that gets published as an OSGi service. It uses a
 * customized class loader that first attempts to load classes used in fxml from
 * the system bundle (which should succeed for any classes that are part of
 * JavaFX itself) and if that does not succeed, uses the call stack to find
 * other bundles that might have access to the required classes and delegates
 * class loading to those.
 * 
 * @author Michael Bachmann
 */
@Component(scope = ServiceScope.PROTOTYPE, service = FXMLLoader.class)
public class OsgiFxmlLoader extends FXMLLoader {

	private static class OsgiClassLoader extends ClassLoader {

		private BundleContext ctx;

		public OsgiClassLoader(BundleContext ctx) {
			this.ctx = ctx;
		}

		@Override
		public synchronized Class<?> loadClass(String name) throws ClassNotFoundException {

			if (this.ctx == null) {
				throw new ClassNotFoundException("Classloader already closed!");
			}

			Class<?> clazz = null;

			for (Bundle bundle : this.ctx.getBundles()) {
				clazz = loadClassFromBundle(name, bundle);
				if (clazz != null) {
					return clazz;
				}
			}

			throw new ClassNotFoundException(name);
		}

		private Class<?> loadClassFromBundle(String className, Bundle bundle) {

			try {
				return bundle.loadClass(className);
			} catch (ClassNotFoundException e) {
				return null;
			}
		}

		public synchronized void close() {
			this.ctx = null;
		}

	}

	private OsgiClassLoader classLoader;

	@Activate
	public synchronized void activate(BundleContext ctx) {
		this.setClassLoader(this.classLoader = AccessController.doPrivileged(new PrivilegedAction<OsgiClassLoader>() {
			@Override
			public OsgiClassLoader run() {
				return new OsgiClassLoader(ctx);
			}
		}));
	}

	@Deactivate
	public synchronized void deactivate() {
		this.classLoader.close();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		URL location = this.getLocation();
		result = prime * result + ((location == null) ? 0 : location.toExternalForm().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj instanceof OsgiFxmlLoader) {

			URL location = this.getLocation();
			URL otherLocation = ((OsgiFxmlLoader) obj).getLocation();

			if (location == null || otherLocation == null) {
				return location == otherLocation;
			}

			return location.toExternalForm().equals(otherLocation.toExternalForm());
		} else if (obj instanceof FXMLLoader) {
			return obj.equals(this);
		}

		return false;
	}
}