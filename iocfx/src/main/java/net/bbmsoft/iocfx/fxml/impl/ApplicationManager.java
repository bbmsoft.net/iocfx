package net.bbmsoft.iocfx.fxml.impl;

import org.osgi.annotation.versioning.ProviderType;

import javafx.scene.Parent;
import javafx.scene.layout.Region;
import net.bbmsoft.iocfx.Standalone;
import net.bbmsoft.iocfx.Fxml.Application;

/**
 * Helper service interface that allows to retrieve the root node of an
 * Application.
 * 
 * @author Michael Bachmann
 *
 */

@ProviderType
public interface ApplicationManager {

	/**
	 * Returns the root node created for the specified application, if one has been
	 * created yet. May only be called on the JavaFX Application Thread.
	 * 
	 * @param application
	 *            the application which's root node is required
	 * @return the specified application's root node or {@code null} if it has not
	 *         been created or registered with the application manager yet.
	 * @throws IllegalStateException
	 *             when called from any other than the JavaFX Application Thread
	 */
	public Parent getRootNode(Standalone application);
	
	public void removeFxmlApplication(Application fxml);

	public void addFxmlApplication(Application fxml, Region object);

}
