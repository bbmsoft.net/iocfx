package net.bbmsoft.iocfx.view;

import java.util.Locale;

import org.osgi.annotation.versioning.ConsumerType;
import org.osgi.service.component.annotations.Component;

import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 * {@code Views} allow GUI designers to separate parts of the UI into individual
 * {@link Component Components} without having to take care of putting all the
 * parts together manually. A {@code View} knows the ID of its parent
 * {@link Pane} and IoCFX takes care of putting it there.
 * <p>
 * By convention, a {@code View} should be derived from {@link Node} and its
 * designated parent should have that {@code View} as its only child. The
 * parent's ID should be the {@code View's} class name in camel case postfixed
 * with "Parent", i.e. the parent of a View that has the class name MyView
 * should have the ID "myViewParent".
 * <p>
 * The {@code View's} parent must always be a {@link Pane} or any derived class,
 * like a {@link StackPane}, {@link HBox}, etc.
 * <p>
 * If the parent has a different ID or the {@code View} cannot be derived from
 * {@link Node}, its {@link #getParentID()} and/or {@link #getNode()} methods
 * must be overridden accordingly.
 * 
 * @author Michael Bachmann
 *
 */

@ConsumerType
public interface View {

	public default String getParentID() {
		String calssName = this.getClass().getSimpleName();
		StringBuilder sb = new StringBuilder("#");
		sb.append(calssName.substring(0, 1).toLowerCase(Locale.US));
		sb.append(calssName.substring(1));
		sb.append("Parent");
		return sb.toString();
	}

	public default Node getNode() {
		if (this instanceof Node) {
			return (Node) this;
		} else {
			throw new IllegalStateException(
					"When a view is NOT derived from a Node class, its getNode() method must be overridden!");
		}
	}
}
