package net.bbmsoft.iocfx;

import org.osgi.annotation.versioning.ConsumerType;

import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.bbmsoft.iocfx.StageService.ExitPolicy;

/**
 * Base interface for stand-alone applications.
 * 
 * @author Michael Bachmann
 *
 */

@ConsumerType
public interface Standalone {

	/**
	 * Provide an {@code ExitPolicy} for the application's main stage.
	 * 
	 * @return the application's main stage's exit policy
	 */
	public default ExitPolicy getExitPolicy() {
		return ExitPolicy.SHUTDOWN_ON_STAGE_EXIT;
	}

	/**
	 * Customizes the stage before it is shown. Override this e.g. to apply a
	 * {@link StageStyle} before the stage is made visible for the first time.
	 * 
	 * @param primaryStage
	 *            the application's main stage
	 */
	public default void prepareStage(Stage primaryStage) {

	}

	/**
	 * Interface for components that represent an application main window and bring
	 * their own UI layout. The root region provided by this component will
	 * automatically be put into a stage and shown.
	 * <p>
	 * Before the Stage is shown, it will be passed into the
	 * {@link #prepareStage(Stage)} method, giving developers the opportunity to
	 * customize the stage, e.g. to {@link Stage#initStyle(StageStyle) initialize a
	 * stage style}. By default, {@code prepareStage()} does nothing.
	 * <p>
	 * Additionally, an {@code Application} component can provide a customized
	 * {@link ExitPolicy} by overriding the {@link #getExitPolicy()} method. The
	 * default is {@link ExitPolicy#SHUTDOWN_ON_STAGE_EXIT}.
	 * 
	 * @author Michael Bachmann
	 *
	 */
	@ConsumerType
	public interface Application extends Standalone {

		/**
		 * Provide this application's root layout node. This will be put in a stage and
		 * shown. This method will always be called on the JavaFX Application Thread.
		 * <p>
		 * By convention, classes implementing {@link Application} should be derived
		 * from {@link Region}, in which case this method will by default simply return
		 * the {@code Application} instance. If the {@code Application} class is not
		 * derived from {@code Region}, this class must be overridden to return the
		 * correct root node.
		 * <p>
		 * Currently, IoCFX will call this method only once per instance, however this
		 * may change in future versions, so it is advised to make sure that this method
		 * does not create a new layout every time it is called.
		 * 
		 * @return the application layout's root node
		 * @throws IllegalStateException
		 *             if the implementing class is not a {@link Region}
		 */
		public default Region getRootNode() {
			if (this instanceof Region) {
				return (Region) this;
			} else {
				throw new IllegalStateException(
						"Standalone Application classes that do not extend Region must override the getRootNode() method!");
			}
		}
	}
}
